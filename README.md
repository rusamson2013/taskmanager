
# TASK Manager #

The Task Manager is a RESTFUL Web Application that helps you to Create , Edit and Delete Tasks. it also keep track on when was the last time the task was edited.

![URLs Shortener API](images/swagger_main.png)

It also offers a user friendly Front End to login and manage your Tasks.

![URLs Shortener API](images/login_page.png)

### Dependencies
- Java 8 plus
- Tomcat 8 
  
### Tools Used 
 
- **Spring Boot** 
  - For the main application 
- **Maven** 
  - as a build automation tool and packaging as WAR
- **In Memory Spring Security**
  - for both Authentication and authorisation
  
- **JPA - Hibernate** 
  - for perisistance
  
- **H2 Database** 
  - for in memory Data perisistance

- **Junit & Mockito**
  - Testing and mocking objects

- **Sonarlint & Eclipse**
  - Code Quality Check and Tests Coverage
  
- **wagger**
  - for RESTFul Endpoints UI (accessible after login) 
  **http://localhost:8080/swagger-ui.html**

- **Spring Actuator** 
  - for Monitoring (accessible after login) 
  **http://localhost:8080/actuator/index.html**
 
## HOW TO RUN ##


### How to run the APP Using the embedded tomcat as a microservice JAR ###
- from the /app_war or /target folder run the command : **java -jar taskmanager.war**

### How to run the APP from Tomcat as a WAR ###
- Find below steps to Run the project 
	1 - Find the /target folder 
	2 - Drag and drop the WAR file named **taskmanager.war** in a Toomcat **webapps** folder  
	3 - If you tomcat runs on port 8080 , the navigate to the below URL :
   - **http://localhost:8080/taskmanager/app/login**


## USABILITY OF THE SYSTEM ##
below is a short summary on the UI 
### REST interface ###
-  Below are all accessible Endpoints of the application
    -   ![URLs Shortener API](images/swagger_main.png)
- eg.. Here, fields are provided to form requests
    -   ![URLs Shortener API](images/swagger_create.png)
- Access Denied response can also be seen if a user tries to access a on resources without having the privileges
    -   ![URLs Shortener API](images/security.png)
 
### Friendly Web interface ###
 -  For Login
    -   ![URLs Shortener API](images/login_page.png)
-For Errors
    -   ![URLs Shortener API](images/error_page.png)
- Empty form for creating Tasks
    -   ![URLs Shortener API](images/create_page.png)   
 - Display Warnings if some fields are not filled
    -   ![URLs Shortener API](images/create_error.png)     
  -Filled Form
    -   ![URLs Shortener API](images/create.png)     
  -Visualising Created Tasks
    -   ![URLs Shortener API](images/created_task.png)    
  -Editing Existing Tasks
    -   ![URLs Shortener API](images/update.png)   

### Security
  - The security is managed in Spring

### Testing  
  - The full Unit Test Coverage is at 83.7 %
    -  ![URLs Shortener API](images/tests_coverage.png)   
    
### Monitoring Interface 
  - Spring Actuator for Monitoring
    -  ![URLs Shortener API](images/actuator.png)   

### Documentation
  - Java Documentation
    -  ![URLs Shortener API](images/javadoc.png)   
    
## CONFIGURATION ##

You may need to change add more Security Settings or Other Changes

### SECURITY configuration
you will find a file that manages Spring InMemory security under **com.samson.security** Package

### H2 Database configuration

By Default ,I had to set the database to be in memory to avoid the write access issues that may rise. 
If there is a need of having it saved on the local machine , you can change the **application.properties** file located in the Resources folder as below 

  from  :   **spring.datasource.url=jdbc:h2:mem:urlsdb;DB_CLOSE_ON_EXIT=FALSE**
  To    :   **spring.datasource.url=jdbc:h2:file:/data/urlsdb**

## ADDITION INFO ##

the java doc can be found user the /doc folder 