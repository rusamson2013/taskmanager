<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
  <c:url value="/css/main.css" var="jstlCss" />
  <link href="${jstlCss}" rel="stylesheet" />
</head>
<body>
  
<div class="form-style-8"> 
  <h2>Login to your Task Manager</h2>
  <form name='form' action="login" method='POST'>
    <input type="text" name="username" placeholder="Username" required/>
    <input type="password" name="password" placeholder="Password" required/> 
    <input type="submit" value="Submit" />
  </form>
</div>

<div class="form-style-8"> 
   <p>
   Note :In memory user name and password  : 
   <br>
   username : <b>admin</b>  password : <b>adminpass</b> 
   </p>
   </div>
   
</body>
</html>
   