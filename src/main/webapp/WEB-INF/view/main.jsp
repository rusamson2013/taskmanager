<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

 
 <html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
  <c:url value="/css/main.css" var="jstlCss" />
  <link href="${jstlCss}" rel="stylesheet" />
</head>
<body> 

        

 
 
<div class="form-style-8"> 
          <form action="/logout" method="post">
            <input   type="submit" class="float-right" value="Sign Out"/>
        </form>
        <hr>
  <h2>Logged in as : ${pageContext.request.userPrincipal.name} </h2>
  <div id="warning" class="alert alert-warning" ></div>
  Create - update and visualize tasks (Only an Admin has the right to delete a Task)
  <hr>
  <form name='form' action="#">
    <input type="text" name="id" id="id" value='' placeholder="ID :"/>
    <input type="text" name="title" id="title"  placeholder="Title :"/>
    <input type="text" name="description" id="description" placeholder="Description :"/>    
    <br>
    <input type="checkbox" id="checked"  name="checked" >
    <br>
    <br>
    <input type="submit" id="create" value="CREATE" />
    <input type="submit" id="update" value="UPDATE" />
    <input type="submit" id="delete" value="DELETE" />  
    <input type="submit" id="clear" onclick="clearform()" class="float-right" value="CLEAR" />         
  </form>
     
</div>
 
 <div class="form-style-8"> 
 <div id="tasks"></div> 
 </div> 
</body>
</html>
 
<script>

//$(document).ready(function() {

	var getUrl = window.location;
	
	var base_url = window.location.origin;
	 
	
    $.get("/task/all", function(data, status) {
        var result = "<hr>";
        for (var task in data) {

            if (data[task]['checked']) {
                result += '<input type="checkbox" class="float-right" name="checked" checked>';
            } else {
                result += '<input type="checkbox" class="float-right" name="checked">';
            }
            result += "<h2>  " + data[task]['id'] + "  - ";
            result += data[task]['title'] + " </h2>  ";
            result += data[task]['description'] + " </br><hr> Created by  ";
            result += data[task]['owner'] + " </br> ";
            result +=' <input type="submit"  value="Edit"  class="float-right" onclick="displayEdit(' + data[task]['id'] + ')" /> <br><hr> Last Updated :';           
            result += new Date(data[task]['updated']) + " </br><hr> ";
        }
        $("#tasks").html(result);
    });

//});

function displayEdit(id) {
    $.get("/task/" + id, function(data, status) {
        $("#id").val(data['id']);
        $("#title").val(data['title']);
        $("#description").val(data['description']);
        $("#checked").prop('checked', data['checked']);
    });
}

$("#create").click(function() {

    var title = $("#title").val();
    var description = $("#description").val();
    
    if(title =="" || description == "" ){
    	$("#warning").show();
    	$("#warning").html("The Title and Description should not be empty !"); 
    	return;
    }
    
    var datatosend = '{"id":0,"title":"' + title + '","description":"' + description + '","checked":' + $("#checked").prop('checked') + ',"owner":"","updated":0}';
    var obj = JSON.parse(datatosend);	
    $.ajax({
            url: '/task/add',
            method: 'POST',
            data: obj
        })
        .then(function(data, status) {
        	location.reload();
        });
});

$("#update").click(function() {

    var id = $("#id").val();
    var title = $("#title").val();
    var description = $("#description").val();

    if(id ==""){   	
    	$("#warning").show();
    	$("#warning").html("The ID should not be empty !");  
    	return;
    }
    
    var datatosend = '{"id":' + id + ',"title":"' + title + '","description":"' + description + '","checked":' + $("#checked").prop('checked') + ',"owner":"","updated":0}';
    var obj = JSON.parse(datatosend);
    //alert(obj);		
    $.ajax({
            url: '/task/update',
            method: 'PUT',
            data: obj
        })
        .then(function(data, status) {
        });
});

$("#delete").click(function() {

    var id = $("#id").val(); 

    if(id ==""){
    	$("#warning").show();
    	$("#warning").html("The ID should not be empty !"); 
    	return;
    }
    
    $.ajax({
            url: '/task/delete/'+id,
            method: 'DELETE'
        })
        .then(function(data, status) {
             alert(data);
        });
});

function clearform() {
    $("#id").val("");
    $("#title").val("");
    $("#description").val("");
    $("#checked").prop('checked', false);
}

$("#warning").hide();
</script>
   