package com.samson; 

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
* <h1>Task Manager Application</h1>
*This API allows to users to record a list of tasks
* @author  Samson Rukundo
* @version 1.0
* @since   2021-03-14
*/

@SpringBootApplication
public class SpringBootWebApplication extends SpringBootServletInitializer {
	  
    private static Logger loggerInfo = LoggerFactory.getLogger(SpringBootWebApplication.class);

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SpringBootWebApplication.class);
	}

	public static void main(String[] args){
		SpringApplication.run(SpringBootWebApplication.class, args);
		loggerInfo.info("Application started");
	}

}