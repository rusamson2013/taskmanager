package com.samson.security;
 
import org.springframework.context.annotation.Configuration; 
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder; 
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter; 

/**
* <h1>InMemoryAuth Configuration</h1>
* This is the main configuration for inMemory Authentication
* 
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

@Configuration
@EnableWebSecurity
public class InMemoryAuthWebSecurityConfigurer 
  extends WebSecurityConfigurerAdapter { 
  
	private static final String ADMIN = "ADMIN";
	
    // Securing the urls and allowing role-based access to these urls.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests() 
        .antMatchers("/*").hasAnyRole(ADMIN)
        .and().csrf().disable();
         
        http
        .authorizeRequests()
            .antMatchers("/resources/**", "/app/", "/webapp/**").permitAll()
            .anyRequest().authenticated()
            .and()
        .formLogin() 
        	.loginPage("/app/login")               
        	.permitAll()
        	.failureUrl("/app/errorpage")
        	.defaultSuccessUrl("/app/main")
            .and()
        .logout()
            .permitAll();
        
    }
   
    // In-memory authentication to authenticate the user i.e. the user credentials are stored in the memory.
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception { 
        auth.inMemoryAuthentication().withUser("admin").password("adminpass").roles(ADMIN);
    }
	  
}
