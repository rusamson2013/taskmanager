package com.samson.model;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp; 

/**
* <h1>TASK Entity</h1>
* This is will be used for persistence and has also one namedQuery
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

@Entity 
@NamedQueries({
    @NamedQuery(name="findAll",
                query="SELECT t FROM Task t"), 
}) 
@Table(name = "Task")
public class Task { 
@Id 
@GeneratedValue(strategy = GenerationType.AUTO)
private int id; 
private String title;
private String description;  
private boolean checked;  
private String owner; 
@UpdateTimestamp
private Date updated;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public Date getUpdated() {
	return updated;
}
 
public boolean isChecked() {
	return checked;
}

public void setChecked(boolean checked) {
	this.checked = checked;
}

public String getOwner() {
	return owner;
}

public void setOwner(String owner) {
	this.owner = owner;
}


 
}
