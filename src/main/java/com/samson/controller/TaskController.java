package com.samson.controller;
 
import java.util.List; 
 
import javax.validation.Valid;
  
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping; 
  
import com.samson.model.Task;
import com.samson.service.TaskService; 

/**
* <h1>Main Controller</h1>
* This is the tasks controller
* it gives an entry point to all tasks operations 
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

@Controller
@RequestMapping("task")
public class TaskController { 
	
	@Autowired
	private TaskService taskService;    

	   /**
	   * This method is used to create a task and return it. .  
	   * @param Task, This the Task object
	   * @param model, This is the model object
	   * @return Task, Return the freshly created task .
	   */
	
    @PostMapping(value = "/add", produces = "application/json")
	public ResponseEntity<Task>  createTask(@Valid @ModelAttribute("Task") Task task) {  
		return new ResponseEntity<>( taskService.create(task), HttpStatus.OK);
 	}
    
	   /**
	   * This method returns all Tasks from the DB.  
	   * @return List, Return the List of all Tasks .
	   */  
    @GetMapping(value = "/all", produces = "application/json")
	public ResponseEntity<List<Task>> getAll() { 
	return new ResponseEntity<>(taskService.getAllTasks(), HttpStatus.OK);
	} 
 
	   /**
	   * This method is used to get a Task by id.  
	   * @param id, This is the id of the task  
	   * @return Task, Returns the requested Task.
	   */
     @GetMapping("/{id}")
    public ResponseEntity<Task> getTask(@PathVariable("id") int id) {  
    	 return new ResponseEntity<>( taskService.get(id), HttpStatus.OK);
    }   
 
	   /**
	   * This method is used to update a task and return it. .  
	   * @param Task, This the Task object
	   * @param model, This is the model object
	   * @return Task, Return the freshly created task .
	   */
	
     @PutMapping(value = "/update", produces = "application/json")
	public ResponseEntity<Task>  updateTask(@Valid @ModelAttribute("Task") Task task) {  
		return new ResponseEntity<>( taskService.update(task), HttpStatus.OK);
	}
  
	  /**
	  * This method is used to update a task and return it. .  
	  * @param Task, This the Task object
	  * @param model, This is the model object
	  * @return Task, Return the freshly created task .
	  */
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")
	public  void deleteTask(@PathVariable("id") int id) {  
		  taskService.delete(id);
	}

}