package com.samson.controller;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
 
/**
* <h1>Main Controller</h1>
* This is the main Controller
* it gives the entry point to the system
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

@Controller
@RequestMapping("app")
public class MainController { 
	
	   /**
	   * This method is used to access the default page (index).  
	   * @param model, This is the model
	   * @return String, This is the name of the default page.
	   */
	@RequestMapping("/login")
	public String loginPage(Map<String, Object> model) {   
	    return "login";
	} 
	
	   /**
	   * This method is used to access the main page (main).  
	   * @param model, This is the model
	   * @return String, This is the name of the main page.
	   */
	
	@RequestMapping("/main")
	public String mainPage(Map<String, Object> model) {   
	    return "main";
	} 

	   /**
	   * This method is used to access the error page (error).  
	   * @param model, This is the model
	   * @return String, This is the name of the error page.
	   */
	@RequestMapping("/errorpage")
	public String errorPage(Map<String, Object> model) {   
	    return "errorpage";
	} 
}