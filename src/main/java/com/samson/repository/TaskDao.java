package com.samson.repository;

import java.util.List;

import com.samson.model.Task; 

/**
* <h1>Task Data Access Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

public interface TaskDao {
	
	   /**
	   * This method is used insert a Task to the DB.  
	   * @param Task, This is the only parameter to be used
	   * @return int, This is the ID of the inserted Task.
	   */
	public int insert(Task task);

	   /**
	   * This method is used to return the Task form the DB.  
	   * @param id, This is the ID of the Task that needs to be found
	   * @return Task, This is the found Task.
	   */
	public Task find(int id);

	   /**
	   * This method that update the Task .  
	   * @param task, a task should be passed as the parameter 
	   */
	public void update(Task task);

	   /**
	   * This method is used to delete a Task from the DB.  
	   * @param task, you pass in the task that needs to be deleted 
	   */
	public void delete(Task task); 
	
	   /**
	   * This method is used to return all Tasks existing in the DB.   
	   * @return List, This is the List of all Task from the DB.
	   */
	public List<Task> findAll();
}
