package com.samson.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.samson.model.Task; 

/**
* <h1>Task Data Access Class</h1>
* This deals with the persistence 
* It takes  care of all the DB CRUIDs 
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

@Repository
@Transactional
public class TaskDaoImpl  implements TaskDao {
	
	private  Logger logger = LoggerFactory.getLogger(TaskDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	   /**
	   * This method is used insert a Task to the DB.  
	   * @param task, This is the only parameter to be used
	   * @return int, This is the ID of the inserted Task.
	   */
	@Override	
	public int insert(Task task) {
	    logger.info("persisting a Task to the DB");
		entityManager.persist(task);
		return task.getId();
	}

	   /**
	   * This method is used to return a Task form the DB.  
	   * @param id, This is the ID of the Task that needs to be found
	   * @return Task, This is the found Task.
	   */
	@Override
	public Task find(int id) {
		logger.info("finding a Task by id");
		return entityManager.find(Task.class, id);
	}

	   /**
	   * This method update a Task .  
	   * @param task, The task should be passed as the parameter 
	   */
	@Override
	public void update(Task task) {
		logger.info("Updating the Task");
		entityManager.merge(task);  
 	}

	   /**
	   * This method is used to delete a Task from the DB.  
	   * @param task, you pass in the task that needs to be deleted 
	   */
	@Override
	public void delete(Task task) {
		logger.info("deleting a Task");
		if(this.find(task.getId()) != null) {
		 entityManager.remove(task); 	
		}
	} 
	
	   /**
	   * This method is used to return all Tasks existing in the DB.   
	   * @return List, This is the List of all Tasks from the DB.
	   */
	@Override	
	public List<Task> findAll() { 
		    logger.info("finding all Tasks from the DB");
			Query query = entityManager.createNamedQuery("findAll", Task.class);
		return query.getResultList();
	}


}