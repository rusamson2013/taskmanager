package com.samson.service;

import java.util.List;

import com.samson.model.Task;

/**
* <h1>Task Service Interface</h1>
* This gives a list of methods that will need to be implemented
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

public interface TaskService {

	/**
	* This method is used to create a Task.  
	* @param Task, The task to save to the DB
	* @return Task, This is the freshly saved Task.
	*/
	public Task create(Task task);
	
	/**
	* This method is used to return a Task based on the id.  
	* @param int, This is the id of the task
	* @return Task, This is the freshly updated Task.
	*/
	public Task update(Task task);
	
	/**
	* This method is used to return a Task based on the id.  
	* @param int, This is the id of the task
	* @return Task, This is the Task.
	*/
	public Task get(int id);

	/**
	* This method is used to delete a Task.  
	* @param id, This is the id of the task to delete
	* @return boolean, if successful or not.
	*/	
	public void delete(int id);

	/**
	* This method is used to return all Tasks.  
	* @return List, This is the list of all tasks.
	*/
	public List<Task> getAllTasks();

}
