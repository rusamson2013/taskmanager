package com.samson.service;
 
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
 
import com.samson.model.Task;
import com.samson.repository.TaskDao; 
/**
* <h1>Task Service Class</h1>
* This class is for the servicing capabilities 
* It takes  care of all the Business Logic
* 
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

@Service 
@Transactional
public class TaskServiceImpl implements TaskService {
    
	@Autowired
	private TaskDao taskDao;
	 
    
	/**
	 * This method is used to save a new Task.  
	 * @param task, This is the only parameter to be used
	 * @return Task, This is the freshly created Task.
	 */
	@Override
	public Task create(Task task) {    
		task.setOwner(SecurityContextHolder.getContext().getAuthentication().getName());
		taskDao.insert(task);
		return task;
	} 

	/**
	 * This method is used to update a given Task.  
	 * @param task, This is the only parameter to be used
	 * @return Task, This is the freshly updated Task.
	 */
	@Override
	public Task update(Task task) {
		task.setOwner(SecurityContextHolder.getContext().getAuthentication().getName());
		taskDao.update(task);
		return this.get(task.getId());
	}

	/**
	 * This method is used to return a Task based on the ID.  
	 * @param id, This is the only parameter to be used
	 * @return Task, This is the Task returned.
	 */
	@Override
	public Task get(int id) { 
		return taskDao.find(id);
	}

	/**
	 * This method is used to delete a Task.  
	 * @param id, This is the id of the task to be deleted
	 */
	@Override
	public void delete(int id) {
		taskDao.delete(taskDao.find(id));
	}

	/**
	 * This method is used to get All tasks from the DB.  
	 * @return List, the list of all tasks.
	 */
	@Override
	public List<Task> getAllTasks() {
		return taskDao.findAll();
	}  
 
}
