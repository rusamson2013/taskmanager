package com.samson.tests;

import java.util.ArrayList;
import java.util.List;

import com.samson.model.Task;

/**
* <h1>Tests Helper</h1>
* This class will be used to provide some functionalities to our tests
* @author  Samson Rukundo
* @version 1.0
* @since   13-03-2021
*/

public class TestHelper {
    
	//a static method to provide a task
	public static Task getTask() {
		Task task = new Task();
		task.setTitle("title");
		task.setDescription("description");
		task.setOwner("owner");
		task.setChecked(true); 
		return task;
	}
	
	//a static method to provide a list of tasks
	public static List<Task> getManyTasks() {		
		Task task1 = new Task();
		task1.setTitle("title1");
		task1.setDescription("description1");
		task1.setOwner("owner1");
		task1.setChecked(true);  
		
		Task task2 = new Task();
		task2.setTitle("title2");
		task2.setDescription("description2");
		task2.setOwner("owner2");
		task2.setChecked(false);  

		Task task3 = new Task();
		task3.setTitle("title3");
		task3.setDescription("description3");
		task3.setOwner("owner3");
		task3.setChecked(true);  
		
		List<Task> tasks = new ArrayList<Task>();
		
		tasks.add(task1);
		tasks.add(task2);
		tasks.add(task3);
		
		return tasks;
	}
	
}
