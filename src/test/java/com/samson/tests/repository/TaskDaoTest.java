package com.samson.tests.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;


import org.junit.Test; 
import org.junit.runner.RunWith; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager; 
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
 
import com.samson.model.Task;
import com.samson.repository.TaskDao;
import com.samson.tests.TestHelper;  

@RunWith(SpringJUnit4ClassRunner.class) 
@DataJpaTest 
@ComponentScan("com.samson.*")
public class TaskDaoTest {
 
	private  Logger logger = LoggerFactory.getLogger(TaskDaoTest.class);
	
	@Autowired(required=true)
	private TestEntityManager entityManager;

	@Autowired(required=true)
	private TaskDao taskDao;   
	
	@Test
	public void testInsertOkay(){   
		Task task =  TestHelper.getTask();
		int savedTaskId = taskDao.insert(task); 
		Task savedTask =  entityManager.find(Task.class, savedTaskId);
		assertThat(task).isEqualTo(savedTask);	
	} 
	
	@Test
	public void testFindOkay(){   
		Task task =  TestHelper.getTask();
		int savedTaskId = taskDao.insert(task);
		Task savedTask =  taskDao.find(savedTaskId);
		assertThat(task).isEqualTo(savedTask); 	
	} 	

	@Test
	public void testUpdateOkay(){   
		Task task =  TestHelper.getTask();
		int savedTaskId = taskDao.insert(task);
		Task savedTask =  taskDao.find(savedTaskId);
		savedTask.setTitle("new title"); 
		taskDao.update(savedTask);
		savedTask =  taskDao.find(savedTaskId);
		assertThat(savedTask.getTitle()).isEqualTo("new title"); 
		assertThat(task.getOwner()).isEqualTo(savedTask.getOwner()); 	
		assertThat(savedTaskId).isEqualTo(savedTask.getId());  
	}

	@Test
	public void testDeleteOkay(){   
		Task task =  TestHelper.getTask();
		int savedTaskId = taskDao.insert(task);
		taskDao.delete(task);
		Task deletedTask = taskDao.find(savedTaskId); 
		assertThat(deletedTask).isNull(); 	
	}

	@Test
	public void testGetAllTasksOkay(){   
		List<Task> tasks = TestHelper.getManyTasks();		
		for (Task task : tasks) {
			taskDao.insert(task); 
		}		
		List<Task> savedTasks = taskDao.findAll();
		int numberOfTasks = savedTasks.size();		
		assertThat(numberOfTasks).isEqualTo(3); 	
	}
	 
}
