package com.samson.tests.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.List;


import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan; 
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import com.samson.model.Task;
import com.samson.service.TaskService;
import com.samson.tests.TestHelper; 

@RunWith(SpringJUnit4ClassRunner.class) 
@DataJpaTest 
@ComponentScan("com.samson.*")
@ExtendWith(MockitoExtension.class)
public class TaskServiceTest {
 
	private  Logger logger = LoggerFactory.getLogger(TaskServiceTest.class);
	

	@Autowired(required=true)
	private TaskService taskService;  
	
	@Before
	public void setupMock() {
	    MockitoAnnotations.initMocks(this);
	    
	 }
	     
	@Test
	public void testTaskCreateOkay(){  		
		
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("user");
		Task task =  TestHelper.getTask();
		Task createdTask = taskService.create(task); 	
		assertThat(createdTask).isEqualTo(task);	
	} 	 

	@Test
	public void testTaskUpdateOkay(){  	
		
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("user");
		
		Task task =  TestHelper.getTask();
		Task createdTask = taskService.create(task); 		
		createdTask.setTitle("new title");
		Task updatedTask = taskService.update(createdTask); 
		assertThat(updatedTask.getTitle()).isEqualTo("new title");	
	} 	 

	@Test
	public void testTaskDeleteOkay(){  				
		Task task =  TestHelper.getTask();
		Task createdTask = taskService.create(task);
		int id = createdTask.getId();
	    taskService.delete(id);
	    Task deletedTask = taskService.get(id);
		assertThat(deletedTask).isNull();	
	} 	
	
	@Test
	public void testGetAllTasksOkay(){  	
		List<Task> tasks = TestHelper.getManyTasks();		
		for (Task task : tasks) {
			taskService.create(task);
		}		
		List<Task> savedTasks = taskService.getAllTasks();
		int numberOfTasks = savedTasks.size();		
		assertThat(numberOfTasks).isEqualTo(3); 	
	} 	
	
}
